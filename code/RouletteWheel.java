import java.util.*;

public class RouletteWheel {
    private int num;
    private Random rand;
    private int color;
    private int EvenOrOdd;

    public RouletteWheel() {
        this.num=0;
        rand = new Random();
    }
        public void spin(){
            this.num = rand.nextInt(37);
            this.color = rand.nextInt(2);
            this.EvenOrOdd = rand.nextInt(2);
        }
        public int getValue(){
            return this.num; 
        }
        public String getColor(){
            if (this.color == 0) {
                return "Green";
            }
            else {
                return "Black";
            }
        }
        public String getEvenOrOdd(){
            if (this.EvenOrOdd == 0) {
                return "Odd";
            }
            else {
                return "Even";
            }
        }
        public boolean hasWon(int userNum, int userColor, int userOddOrEven) {
            if (userNum == this.num && userColor == this.color && userOddOrEven == this.EvenOrOdd) {
                return true; 
            }
            else {
                return false;
            }
        }
}
