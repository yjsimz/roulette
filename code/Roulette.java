import java.util.*;

public class Roulette {
    public static void main (String[] args) {
        RouletteWheel wheel = new RouletteWheel();
        Scanner scan=new Scanner(System.in);
        int currentBalance = 1000;
        int betColor;
        int OddOrEven;
        int income;
        int betNum;
        String ans="";
        int betMoney;
        Boolean wanToContinue = true;
        
        while (wanToContinue) {
            System.out.println("Would you like to make a bet? y/n");
            ans = scan.next();

        if (ans.equals("y")) {
            System.out.println("What number would you like to bet on? (Please choose a number between 0 and 36)");
            betNum = scan.nextInt();
            if (betNum >= 37 || betNum <0) {
                System.out.println("**Number out of range. Please choose a number between 0 and 36**");
            }
            else {
                System.out.println("What color would you like to bet on? 0 (Green) / 1 (Black)");
                betColor = scan.nextInt();
                if(betColor != 0 && betColor != 1) {
                    System.out.println("**Please choose a color between 0 (Green) and 1 (Black)**");
                }
                else {
                    System.out.println("Would you like to bet on Odd or Even number? 0 (Odd) / 1 (Black)");
                    OddOrEven = scan.nextInt();
                    if (OddOrEven != 0 && OddOrEven != 1) {
                        System.out.println("**Please choose between 0 (Odd) and 1 (Even)**");
                    }
                    else {
                        System.out.println("How much would you like to bet?");
                        betMoney = scan.nextInt();
                        if (betMoney>currentBalance) {
                            System.out.println("**You don't have enough money.Please try again with different amount.**");
                        }
                        else {
                            currentBalance = currentBalance - betMoney;
                            System.out.println("Spinning the wheel...");
                            System.out.println("============================================================");
                            
                            wheel.spin();

                            System.out.println("The Wheel spun: " + wheel.getValue());
                            System.out.println("The Color is: " + wheel.getColor());
                            System.out.println("The number is: " + wheel.getEvenOrOdd());
                            if(wheel.hasWon(betNum,betColor,OddOrEven) == true) {
                                System.out.println("You have won!");
                                income = (betMoney * 35) + betMoney;
                                currentBalance = currentBalance + income;
                                System.out.println("You won: $" + income);
                                System.out.println("Your current balance is: $" + currentBalance);
                            }
                            else {
                                System.out.println("You lost:(");
                                System.out.println("You've lost: $" + betMoney);
                                System.out.println("Your current balance is: $" + currentBalance);
                            }
                        }
                    }
                }
            }
        }
        else if(ans.equals("n")) {
            System.out.println("Understandable. Have a nice day");
        }
        else {
            System.out.println("Understandable. Have a nice day.");
        }
            if (currentBalance == 0) {
                System.out.println("Better luck next time!");
                wanToContinue = false;
            }
            else {
                System.out.println("Would you like to go for another round? y/n");
                scan.nextLine();
                String playAgain = scan.nextLine();
                if (playAgain.equals("n")){
                    wanToContinue = false;
                }
            }
        }
    }
}